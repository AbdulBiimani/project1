# ERS_REIMBURSEMENT_SYSTEM
## Project Description
The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.
## Technologies Used
* Javalin - Version 3.10.1
* slf4j - Version 1.7.30
* Log4J - Version 1.2.17
* Java - Version 1.8
* Jackson - Version -2.10.3
* Mariadb - 2.6.2
* JavaScript
* html
* CSS
* Boostrap

## Features
* Able to log into an employee page and submit reimbursements
* Able to log into a manager page view requests submitted by employees and approve or deny.
* Able to log into an employee page and to see status of the reimbursement. 
## Usage
Open the project in a STS style IDE log4jproperties update the log4j.appener.file.File to a file on your local machine.
Run the MainDriver.java. Go to your favorite browser and go to http://localhost:9008/html/index.html.
From there you can log into the reimbursement system as an Employee (e.g. employee username:  Pablo e.g. password: <pablo123>) or as a Financial Manager (e.g. manager username: <Alex> e.g. manager password: <Alex123>)
You will be able to submit reimbursements in the submit new reimbursement page. You can approve/deny reimbursements in 
