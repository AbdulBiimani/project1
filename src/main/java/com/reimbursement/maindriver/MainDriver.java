package com.reimbursement.maindriver;

import com.reimbursement.controller.ReimbursementController;
import com.reimbursement.dao.ReimbursementDao;
import com.reimbursement.dbconnection.ReimbursementDBConnection;
import com.reimbursement.service.ReimbursementService;

import io.javalin.Javalin;

public class MainDriver {
public static void main(String[] args) {
	ReimbursementController rCon = new ReimbursementController(new ReimbursementService(new ReimbursementDao(new ReimbursementDBConnection())));
		Javalin app = Javalin.create(config->{
			config.addStaticFiles("/frontend");
		});
		app.start(9008);
		app.post("/users/login", rCon.postLogin);
		app.get("/users/sessions", rCon.getSessionReimb);
		app.post("/submit", rCon.submitReimb);
		app.get("/view",rCon.viewHistory);
		app.post("/update", rCon.modifyRequests);
		app.get("/viewmanager", rCon.viewnewSubmitted);
		app.get("/logout",rCon.post_logout);
		app.exception(NullPointerException.class,(e,ctx)->{
			ctx.status(404);
			ctx.result("User does not exist");
			
		});
		
		app.exception(NumberFormatException.class, (e,ctx)->{
			e.printStackTrace();
		});
	}
}
