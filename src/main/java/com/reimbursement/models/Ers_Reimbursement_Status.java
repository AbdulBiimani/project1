package com.reimbursement.models;

public class Ers_Reimbursement_Status {

	private int reimb_status_id;
	private String reimb_status;
	public Ers_Reimbursement_Status() {
		// TODO Auto-generated constructor stub
	}
	public Ers_Reimbursement_Status(int reimb_status_id, String reimb_status) {
		super();
		this.reimb_status_id = reimb_status_id;
		this.reimb_status = reimb_status;
	}
	public Ers_Reimbursement_Status(String reimb_status) {
		super();
		this.reimb_status = reimb_status;
	}
	public String getReimb_status() {
		return reimb_status;
	}
	public void setReimb_status(String reimb_status) {
		this.reimb_status = reimb_status;
	}
	public int getReimb_status_id() {
		return reimb_status_id;
	}
	@Override
	public String toString() {
		return "Ers_Reimbursement_Status [reimb_status_id=" + reimb_status_id + ", reimb_status=" + reimb_status + "]";
	}
	
	
	
	
}
