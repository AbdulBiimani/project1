package com.reimbursement.models;

public class Ers_Reimbersement_Type {
	private int reimb_type_id;
	private String reimb_type;
	public Ers_Reimbersement_Type() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Ers_Reimbersement_Type [reimb_type_id=" + reimb_type_id + ", reimb_type=" + reimb_type + "]";
	}
	public Ers_Reimbersement_Type(String reimb_type) {
		super();
		this.reimb_type = reimb_type;
	}
	public String getReimb_type() {
		return reimb_type;
	}
	public void setReimb_type(String reimb_type) {
		this.reimb_type = reimb_type;
	}
	public int getReimb_type_id() {
		return reimb_type_id;
	}

	
}
