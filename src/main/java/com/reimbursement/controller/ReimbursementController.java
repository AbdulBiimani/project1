package com.reimbursement.controller;

import java.util.List;
import java.util.logging.Logger;

import com.reimbursement.models.Ers_Reimbursement;
import com.reimbursement.models.Ers_Users;
import com.reimbursement.service.ReimbursementService;

import io.javalin.http.Handler;

public class ReimbursementController {
	private static Logger log = Logger.getLogger("Mylogger");
	private ReimbursementService rServ;
	public Handler postLogin = (ctx)->{
	//	System.out.println(ctx.body());
		Ers_Users eu = rServ.getErs_username(ctx.formParam("ers_username"));
		if(rServ.passwordVerify(ctx.formParam("ers_username"), ctx.formParam("ers_password"))&(eu.getUser_role_id()==10)) {
			System.out.println("User is verified");
//			System.out.println(rServ.getErs_username(ctx.formParam("ers_username")));		
			ctx.sessionAttribute("usr", rServ.getErs_username(ctx.formParam("ers_username")));	
			ctx.redirect("/html/Emp_submit.html");
			log.info("employee logged in");
		}else if(eu.getUser_role_id()==20){
			ctx.redirect("/html/financialmanager.html");
			log.info("Manager logged in");
		}else
		{ System.out.println("User is not verified");
		  ctx.result("wrong username or password");
		  log.info("User denied log-in");
		  ctx.redirect("/html/index.html");
		}  
	};
	
	public Handler getSessionReimb=(ctx)->{
//	System.out.println((Ers_Users)ctx.sessionAttribute("usr"));	
	Ers_Users usr = (Ers_Users)ctx.sessionAttribute("usr");
	ctx.json(usr);
	
	};
	
	public  Handler post_logout =(ctx) -> {
		ctx.sessionAttribute("user", null);
		log.info("User logged out");
		ctx.redirect("/html/index.html");
	};
	
	
	public Handler submitReimb=(ctx)->{
//		int x;
//	Ers_Users eu = rServ.getErs_username(ctx.formParam("ers_users_id"));
//	Ers_Reimbursement reimb = ctx.bodyAsClass(Ers_Reimbursement.class);
//	System.out.println(ctx.json(reimb));
//		x = eu.getErs_users_id();
		 
//		  
		  String val1 =ctx.formParam("reimb_amount").replaceAll("\\s+", "");
		  String val2 = ctx.formParam("reimb_type_id").replaceAll("\\s+", "");
		  String val3 = ctx.formParam("reimb_author").replaceAll("\\s+", "");
		  if(val1==""|val2==""|val3=="") {
			  ctx.result("invalid input");
			  ctx.redirect("/html/Emp_submit.html");
			 
		  }
		 
		  int reimb_amount =Integer.parseInt(val1);
		  int reimb_type_id = Integer.parseInt(val2);
		  int reimb_author = Integer.parseInt(val3);
		  String reimb_description = ctx.formParam("reimb_description");
//    System.out.println(reimb_amount);
//    System.out.println(reimb_type_id);
//    System.out.println(reimb_author);
//    System.out.println(reimb_description);
   
//		Ers_Reimbursement reimb = new Ers_Reimbursement(reimb_amount,reimb_description,reimb_type_id);
//		 Ers_Reimbursement reimb = ctx.bodyAsClass(Ers_Reimbursement.class);
					
	//	System.out.println("the following is reimb" + reimb);
		
	//	Ers_Users eu = ctx.bodyAsClass(Ers_Users.class);
		if(reimb_author!=0) {
			rServ.submit_reimb(reimb_amount,reimb_description,reimb_author,reimb_type_id);
		}	
		ctx.redirect("/html/Emp_submit.html");
	};
	
	 public Handler viewHistory=(ctx)->{
//		 List<Ers_Reimbursement> data = new ArrayList<>(); 
		 List<Ers_Reimbursement> viewStatus = rServ.viewStatus();
		 ctx.json(viewStatus);
		 
		 
		
		 
	 };
	
	 public Handler modifyRequests = (ctx)->{
		 String val1 =ctx.formParam("reimb_id").replaceAll("\\s+", "");
		 String val2 = ctx.formParam("reimb_status_id").replaceAll("\\s+", "");
		 int reimb_id =Integer.parseInt(val1);
		 int reimb_status_id  = Integer.parseInt(val2);
		 System.out.println(reimb_id);
		 System.out.println(reimb_status_id);
		 if((reimb_id !=0) & (reimb_status_id!=0)) {
	   rServ.ApproveDeny(reimb_id,reimb_status_id);
		 }
		 ctx.redirect("/html/financialmanager.html");
		 
	 };
	 
	 public Handler viewnewSubmitted = (ctx)->{
		 
		 List<Ers_Reimbursement> managerviewStatus = rServ.managerviewStatus();
		 ctx.json(managerviewStatus);
	 };
	public ReimbursementController() {
		
	}
	
	

	public ReimbursementController(ReimbursementService rServ) {
		super();
		this.rServ=rServ;
	}

}
