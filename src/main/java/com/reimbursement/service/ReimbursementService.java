package com.reimbursement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.reimbursement.dao.ReimbursementDao;
import com.reimbursement.models.Ers_Reimbursement;
import com.reimbursement.models.Ers_Users;

public class ReimbursementService {
	
private ReimbursementDao rDao;
	
	public ReimbursementService() {
		
	}

	public void submit_reimb( 
								  int reimb_amount , String reimb_description ,int reimb_author,int
								  reimb_type_id
								 ) {
	//	reimb.setReimb_author(eu.getErs_users_id());
	rDao.create(  reimb_amount , reimb_description ,reimb_author,reimb_type_id );
		
	}
	
	public List<Ers_Reimbursement> viewStatus() {
		List<Ers_Reimbursement> li = new ArrayList<>();
		li=rDao.getAll();
		
		return li;

	}
	
	public void ApproveDeny(int reimb_id, int reimb_status_id) {
		
		rDao.update(reimb_id, reimb_status_id);
	}
	public List<Ers_Reimbursement> managerviewStatus() {
		List<Ers_Reimbursement> lisman = new ArrayList<>();
		lisman =rDao.getAllnew();
		
		return lisman;

	}
	
       public ReimbursementService(ReimbursementDao rDao) {
		super();
		this.rDao = rDao;
	}

 public Ers_Users getErs_username(String ers_username) {
	 Ers_Users usr = rDao.findByName(ers_username);
	 
	 if(usr == null) {
		 throw new NullPointerException();
	 }
	 return usr;
 }

 public boolean passwordVerify(String ers_username, String ers_password) {
	 boolean isVerified = false;
	 Ers_Users usr = getErs_username(ers_username);
	 if(usr.getErs_password().equals(ers_password)) {
		 isVerified=true;
	 }
	 return isVerified;
 }
 
 



}
