package com.reimbursement.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ReimbursementDBConnection {
	
	private static String url = "jdbc:mariadb://database-1.cwc10dem6j6l.us-east-2.rds.amazonaws.com:3306/project1";
	private static String  username = "project1user";
	private static String password = "mypassword";
	
	public Connection getDBConnection() throws SQLException{
		
		return DriverManager.getConnection(url,username,password);
		
	}


}
