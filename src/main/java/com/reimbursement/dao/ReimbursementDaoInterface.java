package com.reimbursement.dao;

import java.util.List;

import com.reimbursement.models.Ers_Reimbursement;
import com.reimbursement.models.Ers_Users;

public interface ReimbursementDaoInterface {
	
	void create(int reimb_amount , String reimb_description ,int reimb_author,int reimb_type_id);

	
    List<Ers_Reimbursement>  getAll();
	void update(int reimb_id,int reimb_status_id);
	Ers_Users IdentifyUser(int user_role_id);
	Ers_Users findByName(String ers_username);
	List<Ers_Reimbursement> getAllnew();
}
