package com.reimbursement.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.reimbursement.dbconnection.ReimbursementDBConnection;
import com.reimbursement.models.Ers_Reimbursement;
import com.reimbursement.models.Ers_Users;

public class ReimbursementDao implements ReimbursementDaoInterface{
	
	public static ReimbursementDBConnection rdbc;
	

	@Override
	public void create(int reimb_amount,String reimb_description,int reimb_author,int reimb_type_id) {
		 try(Connection con = rdbc.getDBConnection()){
			 String sql = "INSERT into ers_reimbursement(reimb_amount,reimb_description,reimb_author,reimb_type_id) VALUES(?, ?, ? ,?)";
			 PreparedStatement ps = con.prepareStatement(sql);
			 ps.setInt(1,reimb_amount);
			 ps.setString(2,reimb_description);
			 ps.setInt(3,reimb_author);
             ps.setInt(4,reimb_type_id);
             int changed =ps.executeUpdate();
 		//	System.out.println("num of rows changed " + changed);
//			 ResultSet rs = ps.executeQuery();
			
			
		 }catch(SQLException e) {
			 e.printStackTrace();
		 }
		
		 
	}

	@Override
	public List<Ers_Reimbursement> getAll() {
	
		List<Ers_Reimbursement> entities = new ArrayList<>();
		 try(Connection con = rdbc.getDBConnection()){
			 String sql ="select * from ers_reimbursement order by reimb_id";
				PreparedStatement ps = con.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					
	 entities.add(new Ers_Reimbursement(rs.getInt(1),rs.getInt(2),rs.getTimestamp(3),rs.getTimestamp(4),
			rs.getString(5),rs.getBlob(6),rs.getInt(7),rs.getInt(8),rs.getInt(9),rs.getInt(10)));
				}
				
				
			}catch(SQLException e) {
				e.printStackTrace();
			}
		 
		 System.out.println(entities);
		
		 return entities;
		 
		 
	}
	
	@Override
	public List<Ers_Reimbursement> getAllnew() {
	
		List<Ers_Reimbursement> entities = new ArrayList<>();
		 try(Connection con = rdbc.getDBConnection()){
			 String sql ="select * from ers_reimbursement where reimb_status_id=20 order by reimb_submitted;";
				PreparedStatement ps = con.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					
	 entities.add(new Ers_Reimbursement(rs.getInt(1),rs.getInt(2),rs.getTimestamp(3),rs.getTimestamp(4),
			rs.getString(5),rs.getBlob(6),rs.getInt(7),rs.getInt(8),rs.getInt(9),rs.getInt(10)));
				}
				
				
			}catch(SQLException e) {
				e.printStackTrace();
			}
		 
//		 System.out.println(entities);
		
		 return entities;
		 
		 
	}
	
	@Override
	public void update(int reimb_id, int reimb_status_id) {
		try(Connection con = rdbc.getDBConnection()){
	String sql = "update ers_reimbursement set reimb_status_id="+reimb_status_id+" where reimb_id ="+reimb_id;
	         PreparedStatement ps = con.prepareStatement(sql);
            int changed =ps.executeUpdate();
	//		System.out.println("num of rows changed " + changed);
		 }catch(SQLException e) {
			 e.printStackTrace();
		 }
		
		 
	}
		
	
	public ReimbursementDao() {
		
	}
	
	public ReimbursementDao(ReimbursementDBConnection rdbc) {
		this.rdbc = rdbc;
	}



	@Override
	public Ers_Users IdentifyUser(int user_role_id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ers_Users findByName(String ers_username) {
		Ers_Users usr = null;
		 try(Connection con = rdbc.getDBConnection()){
			 String sql = "SELECT * FROM ers_users WHERE ers_username=?";
			 PreparedStatement ps = con.prepareStatement(sql);
			 ps.setString(1,ers_username);
			 ResultSet rs = ps.executeQuery();
			 if(!rs.first()) {
				 return usr;
			 }
			 usr = new Ers_Users(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),
					 rs.getString(6),rs.getInt(7));
			 
		 }catch(SQLException e) {
			 e.printStackTrace();
		 }
		return usr;
	}


}
